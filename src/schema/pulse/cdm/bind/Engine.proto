syntax = "proto3";
package pulse.cdm.bind;
option java_package = "com.kitware.pulse.cdm.bind";
option csharp_namespace = "pulse.cdm.bind";
option optimize_for = SPEED;

import "pulse/cdm/bind/Enums.proto";
import "pulse/cdm/bind/Events.proto";
import "pulse/cdm/bind/Actions.proto";
import "pulse/cdm/bind/Patient.proto";
import "pulse/cdm/bind/PatientActions.proto";
import "pulse/cdm/bind/PatientConditions.proto";
import "pulse/cdm/bind/EnvironmentActions.proto";
import "pulse/cdm/bind/EnvironmentConditions.proto";
import "pulse/cdm/bind/EquipmentActions.proto";
import "pulse/cdm/bind/Properties.proto";

message DecimalFormatData
{
enum eType
{
  SystemFormatting  = 0;/*<<@brief Not provided, let the system do what it wants */
  DefaultFloat      = 1;
  FixedMantissa     = 2;
  SignificantDigits = 3;
}

  eType                                                Type                      = 1;
  uint32                                               Precision                 = 2;
}

message DataRequestData
{
  enum eCategory
  {
    Patient              = 0;
    Physiology           = 1;
    Environment          = 2;
    Action               = 3;
    GasCompartment       = 4;
    LiquidCompartment    = 5;
    ThermalCompartment   = 6;
    TissueCompartment    = 7;
    Substance            = 8;
    AnesthesiaMachine    = 9;
    BagValveMask         = 10;
    ECG                  = 11;
    Inhaler              = 12;
    MechanicalVentilator = 13;
  }
  
  DecimalFormatData                                    DecimalFormat                   = 1;/**<<@brief If writing to a file, specify the decimal formatting */
  eCategory                                            Category                        = 2;/**<<@brief A category assocated with the Name */
  string                                               ActionName                      = 3;/**<<@brief The action name the property is on */
  string                                               CompartmentName                 = 4;/**<<@brief The compartment name the property could be associated with */
  string                                               SubstanceName                   = 5;/**<<@brief The substance name the property could be associated with */
  string                                               PropertyName                    = 6;/**<<@brief The name of the property requested */
  string                                               Unit                            = 7;/**<<@brief If writing to a file, the unit the data will be in. */
}
message DataRequestManagerData
{
  string                                               ResultsFilename                 = 1;
  double                                               SamplesPerSecond                = 2;
  DecimalFormatData                                    DefaultDecimalFormatting        = 3;
  DecimalFormatData                                    OverrideDecimalFormatting       = 4;
  repeated DataRequestData                             DataRequest                     = 5;
  repeated ValidationTargetData                        ValidationTarget                = 6;
}

message DataRequestedData
{
  int32                                                ID                              = 1;/**<< @brief A unique id associated with this engine, required when using pools. */
  bool                                                 IsActive                        = 2;
  repeated double                                      Value                           = 3;
  repeated EventChangeData                             EventChange                     = 4;
  LogMessagesData                                      LogMessages                     = 5;
}

message ValidationTargetData
{
  enum eType
  {
    Mean          = 0;
    Min           = 1;
    Max           = 2;
  }
  eType                                                 Type                            = 1;
  DataRequestData                                       DataRequest                     = 2;
  double                                                RangeMin                        = 3;
  double                                                RangeMax                        = 4;
}

message DataRequestedListData
{
  repeated DataRequestedData                           DataRequested                   = 1;
}

message AnyConditionData
{
  oneof Condition
  {
    AnyPatientConditionData                            PatientCondition                = 1;
    AnyEnvironmentConditionData                        EnvironmentCondition            = 2;
  }
}
message ConditionListData
{
  repeated AnyConditionData                            AnyCondition                    = 1;
}

message AnyActionData
{
  oneof Action
  {
    AdvanceTimeData                                     AdvanceTime                    = 1;
    SerializeStateData                                  Serialize                      = 2;
    OverridesData                                       Overrides                      = 3;
    AnyPatientActionData                                PatientAction                  = 4;
    AnyEnvironmentActionData                            EnvironmentAction              = 5;
    AnyEquipmentActionData                              EquipmentAction                = 6;
  }
}
message ActionListData
{
  repeated AnyActionData                                AnyAction                      = 1;
}

message ActionMapData
{
  map<int32, ActionListData>                            ActionMap                      = 1;
}

// message DataSetsData // This could hold all on disk data sets (Stabilization Criteria, Substances, Stabilization, Nutrition, Environments, ECG, patients, etc.)
// At this point I am fine with having to have files on disk to create a new patient configuration

message PatientConfigurationData
{
  string                                                DataRoot                       = 1;
  oneof PatientType
  {
    PatientData                                         Patient                        = 2;
    string                                              PatientFile                    = 3;
  }
  ConditionListData                                     Conditions                     = 4;
}

message EngineInitializationData
{
  int32                                                 ID                             = 1;/**<< @brief A unique id associated with this engine, required when using pools. */
  oneof StartType
  {
    PatientConfigurationData                            PatientConfiguration           = 2;/**<< @brief Create an engine using a patient configuration. */
    string                                              StateFilename                  = 3;/**<< @brief Create an engine using a patient state file. */
    string                                              State                          = 4;/**<< @brief Create an engine using a patient state.*/
  }
  DataRequestManagerData                                DataRequestManager             = 5;/**<< @brief Data Requests to use for this engine*/
  string                                                LogFilename                    = 6;/**<< @brief Log file to create for engine. */
  bool                                                  KeepLogMessages                = 7;/**<< @brief Keep log messages from engine, used when building DataRequested objects. */
  bool                                                  KeepEventChanges               = 8;/**<< @brief Keep event changes from engine, used when building DataRequested objects. */
}

message EngineInitializationListData
{
  repeated EngineInitializationData                     EngineInitialization           = 1;/**<< @brief Create an engine for each provided configuration. */
}

/** @brief Static timed stabilization lengths */
message TimedStabilizationData
{
  eSwitch                                               TrackingStabilization          = 1;/**<< @brief Track data requests to the output file (if provided) during stabilization. */
  ScalarTimeData                                        RestingStabilizationTime       = 2;/**<< @brief */
  ScalarTimeData                                        FeedbackStabilizationTime      = 3;/**<< @brief */
  map<string,ScalarTimeData>                            ConditionStabilization         = 4;/**<< @brief */
}

/** @brief A properties target percent difference allowed */
message DynamicStabilizationPropertyConvergenceData
{
  DataRequestData                                       DataRequest                     = 1;/**<< @brief */
  double                                                PercentDifference               = 2;/**<< @brief */
}
/** @brief A properties convergence timing */
message DynamicStabilizationEngineConvergenceData
{
  ScalarTimeData                                        ConvergenceTime                 = 1;/**<< @brief */
  ScalarTimeData                                        MinimumReactionTime             = 2;/**<< @brief */
  ScalarTimeData                                        MaximumAllowedStabilizationTime = 3;/**<< @brief */
  repeated DynamicStabilizationPropertyConvergenceData  PropertyConvergence             = 4;/**<< @brief */
}
/** @brief Dynamic stabilization parameters */
message DynamicStabilizationData
{
  eSwitch                                               TrackingStabilization           = 1;/**<< @brief Track data requests to the output file (if provided) during stabilization. */
  DynamicStabilizationEngineConvergenceData             RestingConvergence              = 2;/**<< @brief */
  DynamicStabilizationEngineConvergenceData             FeedbackConvergence             = 3;/**<< @brief */
  map<string,DynamicStabilizationEngineConvergenceData> ConditionConvergence            = 4;/**<< @brief */
}

message LogMessagesData
{
  repeated string                                       DebugMessages                   = 1;
  repeated string                                       InfogMessages                   = 2;
  repeated string                                       WarningMessages                 = 3;
  repeated string                                       ErrorMessages                   = 4;
  repeated string                                       FatalMessages                   = 5;
}

